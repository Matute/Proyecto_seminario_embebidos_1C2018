#include "modulo_control.h"

struct{
	uint8_t state;
	// otras variables internas
}control_t;

control_IO_t oControlIO;
static control_t oControl;

FSM_Control_State_1(){
	...
}

FSM_Control(){
	switch(oControl.state){
		case 1: FSM_Control_State_1(); break;
		case 2: FSM_Control_State_2(); break;
		...
		case n: FSM_Control_State_n(); break;
	}
}