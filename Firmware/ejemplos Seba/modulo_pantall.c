#include "modulo_pantalla.h"

typedef enum{
	PANTALLA_ESTADO_ENCENDIDA,
	PANTALLA_ESTADO_APAGADA,
}pantalla_estado_t;

typedef struct{
	// variables de entrada y salida
	pantalla_estado_t estado;
}pantalla_IO_t;

struct{
	uint8_t state;
	// otras variables internas
}pantalla_t;

static pantalla_IO_t oPantallaIO;
static pantalla_t oPantalla;

/// Funciones de IO
void Pantalla_Set_Estado(pantalla_estado_t estado){
	oPantallaIO.estado = estado;
}

pantalla_estado_t Pantalla_Get_Estado(void){
	return oPantallaIO.estado;
}

// Funciones de FSM

FSM_Pantalla_State_1(){
	if(PANTALLA_ESTADO_APAGADA == oPantallaIO.estado){
		// hacer algo
	}
}

FSM_Pantalla(){
	switch(oControl.state){
		case 1: FSM_Pantalla_State_1(); break;
		case 2: FSM_Pantalla_State_1(); break;
		...
		case n: FSM_Pantalla_State_n(); break;
	}
}